const {getCircleArea,checkIfPassed,getAverage,getSum,getDifference,div_check,isOddOrEven, reverseString} = require('../src/util.js');
const {expect,assert} = require('chai');

// text case - a condition we are testing.
// it(stringExplainWhatTheTestDoes,functionToTest)
// assert is used to assert condition for the test to pass. If the assertion fails then the test is considered failed.

// describe() is used to create a test suite. A test is a group of test cases related to one another or tests the same method, data or function.
describe('test_get_area_circle_area',()=> {
	
	it('test_area_of_circle_radius_15_is_706.86',()=> {
		let area = getCircleArea(15)
		assert.equal(area,706.86);
	});

	it('test_area_of_circle_radius_300_is_282744',()=> {
		let area = getCircleArea(300)
		expect(area).to.equal(282744);
	});

});

describe('test_check_if_passed',()=> {
	it('test_25_out_of_30_if_passed',()=> {
		let isPassed = checkIfPassed(25,30)
		assert.equal(isPassed,true);
	})

	it('test_30_out_of_50_is_not_passed',()=> {
		let isPassed = checkIfPassed(30,50)
		assert.equal(isPassed,false);
	})
});

describe('test_get_average',()=> {
	it('test_80_82_84_86_average_is_83',()=>{
		let average = getAverage(80,82,84,86)
		assert.equal(average,83);
	});

	it('test_70_80_82_84_average_is_79',()=>{
		let average = getAverage(70,80,82,84)
		assert.equal(average,79);
	});
});

describe('test_get_sum',()=> {
	it('test_if_the_sum_of_15_and_30_is_45',()=> {
		let sum = getSum(15,30)
		assert.equal(sum,45);
	})

	it('test_if_the_sum_of_25_and_50_is_75',()=> {
		let sum = getSum(25,50)
		assert.equal(sum,75);
	})	
});

describe('test_get_difference',()=> {
	it('test_if_the_difference_of_70_and_40_is_30',()=> {
		let difference = getDifference(70,40)
		assert.equal(difference,30)
	})

	it('test_if_the_difference_of_125_and_50_is_75',()=> {
		let difference = getDifference(125,50)
		assert.equal(difference,75)
	})
});

// ACTIVITY SOLUTION

describe('test_div_check',()=> {
	it('test_if_20_is_divisible_by_5',()=> {
		let isDivisible = div_check(20)
		assert.equal(isDivisible,true);
	});

	it('test_if_14_is_divisible_by_7',()=> {
		let isDivisible = div_check(20)
		assert.equal(isDivisible,true);
	});

	it('test_if_21_is_not_divisible_by_5',()=> {
		let isDivisible = div_check(20)
		assert.notEqual(isDivisible,false);
	});

	it('test_if_25_is_not_divisible_by_7',()=> {
		let isDivisible = div_check(20)
		assert.notEqual(isDivisible,false);
	});
});

describe('test_if_odd_or_even',()=> {
	it('test_if_10_is_even',()=> {
		let isEven = isOddOrEven(10)
		assert.equal(isEven,'even');
	});

	it('test_if_11_is_odd',()=> {
		let isOdd = isOddOrEven(11)
		assert.equal(isOdd,'odd');
	});

	it('test_if_11_is_not_even',()=> {
		let isOdd = isOddOrEven(11)
		assert.notEqual(isOdd,'even');
	});

	it('test_if_10_is_not_odd',()=> {
		let isEven = isOddOrEven(10)
		assert.notEqual(isEven,'odd');
	});
}); 

// STRETCH GOAL

describe('test_reverse_string',()=> {
	it('test_if_hello_when_reverse_is_olleh',()=> {
		let strReverse = reverseString('hello')
		assert.equal(strReverse,'olleh');
	});

	it('test_if_hi_when_reverse_is_ih',()=> {
		let strReverse = reverseString('hi')
		assert.equal(strReverse,'ih');
	});

	it('test_if_hello_when_reverse_is_not_hello',()=> {
		let strReverse = reverseString('hello')
		assert.notEqual(strReverse,'hello');
	});

	it('test_if_hi_when_reverse_is_not_hi',()=> {
		let strReverse = reverseString('hi')
		assert.notEqual(strReverse,'hi');
	});
});