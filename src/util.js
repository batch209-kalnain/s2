/* Sample Functions to Test */

function getCircleArea(radius){

	return 3.1416*(radius**2);

}

function checkIfPassed(score,total){
	return (score/total)*100 >= 75;
}

function getAverage(num1,num2,num3,num4){
	return(num1+num2+num3+num4)/4;
}

function getSum(num1,num2) {
	return num1+num2;
}

function getDifference(num1,num2) {
	return num1-num2;
}

// ACTIVITY SOLUTION

function div_check(num) {
	if(num % 5 == 0){
		return true;
	} else if(num % 7 == 0){
		return true;
	} else {
		return false;
	}
}

function isOddOrEven(num) {
	if(num % 2 == 0) {
		return 'even';
	} else {
		return 'odd'
	}
}

function reverseString(str){
	let newStr = '';
	for(let i = str.length-1; i >= 0 ; i--){
		newStr += str[i]
	}
	return newStr;
}

module.exports = {
	getCircleArea: getCircleArea,
	checkIfPassed: checkIfPassed,
	getAverage: getAverage,
	getSum: getSum,
	getDifference: getDifference,
	div_check: div_check,
	isOddOrEven: isOddOrEven,
	reverseString: reverseString
}